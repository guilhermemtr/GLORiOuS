#include <stdlib.h>

#ifndef __QUEUE_DATA__
#define __QUEUE_DATA__

#define AVG 1000

#define WORK_THRESHOLD 1

/**
 * Makes a group interaction.
 */
#define group_interact(g) (g.counter++)

/**
 * Refreshes the group values.
 */
#define refresh_values(g)					            \
  {								                        \
    g.prev_enqueues = g.num_enqueues;				    \
    g.prev_dequeues = g.num_dequeues;				    \
    g.prev_steals = g.num_steals;				        \
    g.prev_stolen = g.num_stolen;				        \
    g.prev_external_enqueues = g.num_external_enqueues;	\
    g.num_enqueues = 0;					                \
    g.num_dequeues = 0;					                \
    g.num_steals = 0;						            \
    g.num_stolen = 0;						            \
    g.num_external_enqueues = 0;				        \
  }

/**
 * Makes an interaction and tries to refresh the group values.
 */
#define interact_and_check_values(g)		\
  {						                    \
    if(group_interact(g) % AVG == 0)		\
      {						                \
	refresh_values(g);			            \
      }						                \
  }

#define get_previous_work(g) (g.prev_dequeues+g.prev_stolen)
#define get_recent_work(g) (g.num_dequeues+g.num_stolen)

#define get_previous_growth(g) (g.prev_enqueues+g.prev_external_enqueues)
#define get_recent_growth(g) (g.num_enqueues+g.num_external_enqueues)

#define get_previous_steals(g) (g.prev_steals+g.prev_external_enqueues)
#define get_recent_steals(g) (g.num_steals+g.num_external_enqueues)

#define get_work_value(g) ((get_previous_work(g)+get_recent_work(g))>>1)
#define get_growth_value(g) ((get_previous_growth(g)+get_recent_growth(g))>>1)
#define get_steals_value(g) ((get_previous_steals(g)+get_recent_steals(g))>>1)

#define get_ideal_group_size(g) ((get_growth_value(g))/(WORK_THRESHOLD+get_work_value(g)))


/**
 * Adds an enqueue to the statistics.
 */
#define group_enqueue(g)			\
  {						            \
    g.num_enqueues++;				\
    interact_and_check_values(g);	\
  }

/**
 * Adds a dequeue to the statistics.
 */
#define group_dequeue(g)			\
  {						            \
    g.num_dequeues++;				\
    interact_and_check_values(g);	\
  }

/**
 * Adds a got stolen to the statistics.
 */
#define group_getstolen(g)			\
  {						            \
    g.num_steals++;				    \
    interact_and_check_values(g);	\
  }

/**
 * Adds a steal from to the statistics.
 */
#define group_steal(g)			    \
  {						            \
    g.num_stolen++;				    \
    interact_and_check_values(g);	\
  }

/**
 * Adds an external enqueue to the statistics.
 */
#define group_external_enqueue(g)	\
  {								    \
    g.num_external_enqueues++;		\
    interact_and_check_values(g);	\
  }

class QueueData {
public:
    QueueData();
    ~QueueData();


    unsigned int counter;
    int num_enqueues;
    int prev_enqueues;
    int num_dequeues;
    int prev_dequeues;
    int num_steals;
    int prev_steals;
    int num_stolen;
    int prev_stolen;
    int num_external_enqueues;
    int prev_external_enqueues;
};

#endif /* __GROUP__ */
