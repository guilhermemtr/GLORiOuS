#include <vector>
#include <stdlib.h>
#include <cmath>

#ifndef STATISTICS_H
#define STATISTICS_H


class Statistics
{
    public:
        /** Default constructor */
        Statistics(int numTests);
        Statistics(int numTests, float toDiscard);

        void addTime(float time);

        float getAverage();

        float getStandardDeviation();

        /** Default destructor */
        virtual ~Statistics();
    protected:
    private:
        float toDiscard;
        float * times;
        int counter;
        int numTests;
};

#endif // STATISTICS_H
