#include "QueueData.h"

QueueData::QueueData()
{
    counter = 0;
    num_enqueues = 0;
    prev_enqueues = 0;
    num_dequeues = 0;
    prev_dequeues = 0;
    num_steals = 0;
    prev_steals = 0;
    num_stolen = 0;
    prev_stolen = 0;
    num_external_enqueues = 0;
    prev_external_enqueues = 0;
}

QueueData::~QueueData()
{
}
