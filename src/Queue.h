#include <memory>
#include <vector>
#include <mutex>
#include <atomic>

#ifndef QUEUE_H
#define QUEUE_H

//Quando e pequeno da problema por nao haver resize
#define Q_SIZE (1 << 23)

<<<<<<< HEAD
=======
const long int queue_size = Q_SIZE;

>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
class Task;

class Queue
{
    public:
        /** Default constructor */
        inline Queue() {
<<<<<<< HEAD
=======
            queue_data = (Task **) malloc(sizeof(Task *) * queue_size);
>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
            nCurrentEnqueue = 0;
            currentEnqueue = 0;
            currentDequeue = 0;
            data = (Task **) malloc(Q_SIZE * sizeof(Task *));
        }


        inline void enqueue(Task * task) {
            long int pos = __sync_fetch_and_add(&nCurrentEnqueue,1);
<<<<<<< HEAD
            data[pos%Q_SIZE] = task;
=======
            queue_data[pos%Q_SIZE] = data;
>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
            while(!__sync_bool_compare_and_swap(&currentEnqueue,pos,pos+1));
        }

        inline Task * tryDequeue() {
            long int pos = currentDequeue;
            if(pos >= currentEnqueue)
                return nullptr;
<<<<<<< HEAD
            Task * task = data[pos%Q_SIZE];
=======
            Task * data = queue_data[pos%Q_SIZE];
>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
            if(__sync_bool_compare_and_swap(&currentDequeue,pos,pos+1)) {
                return task;
            }
            else return nullptr;
        }


        inline Task * trySteal() {
            return tryDequeue();
        }

        /** Default destructor */
        virtual inline ~Queue() {
<<<<<<< HEAD
            free(data);
=======
            free(queue_data);
>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
        }

    protected:
    private:
<<<<<<< HEAD
        Task ** data;
=======
        Task ** queue_data;
>>>>>>> 96468b98e17537bbaec00a5482c9e1c47feea93a
        volatile long int nCurrentEnqueue;
        volatile long int currentEnqueue;
        volatile long int currentDequeue;
};

#endif // QUEUE_H
