#include <queue>

#include "RandomNumberGenerator.h"
#include "Task.h"
#include "Queue.h"
#include "../utils/WorkGroup.h"
#include "../utils/QueueData.h"

class Scheduler;

#ifndef SCHEDULER_H
#define SCHEDULER_H

class Task;

class Scheduler
{
    public:

        /**
         * Default constructor
         */
        Scheduler(void) {
            this->numQueues = get_nprocs();
            this->queues = new Queue[this->numQueues];
            this->queueStats = new QueueData[this->numQueues];
        }

        /**
         * Schedules a task to the scheduler.
         * @param index <code>long int</code> the workers index.
         * The index depends on the sheduled implemented.
         * @param prng <code>std::shared_ptr<RandomNumberGenerator></code> the random number generator.
         * @param t <code>std::shared_ptr<Task></code> the task to be scheduled.
         */
        virtual void scheduleTask(long int index, RandomNumberGenerator * prng, Task * t) {
            int cpu = sched_getcpu();
            int level = get_best_fitted_level(groupings,get_ideal_group_size(queueStats[cpu]));
            if(!level) {
                queues[cpu].enqueue(t);
                group_enqueue(queueStats[cpu]);
            } else {
                long int * queue_indexes = get_queues(groupings,level,cpu);
                int level_size = get_group_size(groupings,level);
                long int pos = prng->frandom()%level_size;
                queues[queue_indexes[pos]].enqueue(t);
                group_enqueue(queueStats[pos]);
                group_external_enqueue(queueStats[cpu]);
            }
        }

        /**
         * Tries to get a scheduled task.
         * @param index <code>long int</code> the workers index.
         * @param prng <code>std::shared_ptr<RandomNumberGenerator></code> the random number generator.
         * @return <code>std::shared_ptr<Task></code> the retrieved task from the scheduler.
         * May return <code>nullptr</code> if currently there aren't any tasks.
         */
        virtual Task * getScheduledTask(long int index, RandomNumberGenerator * prng) {
            int cpu = sched_getcpu();
            Task * t = queues[cpu].tryDequeue();
            if(t != nullptr) {
                group_dequeue(queueStats[cpu]);
                return t;
            }
            int curr_level_index = 0;
            long int seed = prng->frandom();
            while(curr_level_index != get_num_distinct_levels(groupings)) {
                int level_size = get_group_size(groupings,get_distinct_levels(groupings)[curr_level_index]);
                int tries = (level_size + 1)>>1;
                long int qrandom = seed;
                long int * queue_indexes = get_queues(groupings,get_distinct_levels(groupings)[curr_level_index],cpu);
                while(tries != 0) {
                    int pos = qrandom%((long int)level_size);
                    t = queues[queue_indexes[pos]].trySteal();
                    if(t != nullptr) {
                        group_getstolen(queueStats[queue_indexes[pos]]);
                        group_steal(queueStats[cpu]);
                        return t;
                    }
                    qrandom = qrandom + 1 < 0 ? 0 : qrandom + 1;
                    tries--;
                }
                curr_level_index++;
            }
            return NULL;
        }

        /**
         * Default destructor
         */
        virtual ~Scheduler(void) {
            delete [] this->queueStats;
            delete [] this->queues;
        }

    protected:
    private:

        //The number of queue.
        int numQueues;

        ///TO FIX
        //The queue usage statistics
        QueueData * queueStats;

        //The work groups
        Workgroup groupings;

        //The queues.
        Queue * queues;


};

#endif // SCHEDULER_H
