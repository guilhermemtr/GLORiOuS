#include <sys/sysinfo.h>
#include <memory>
#include <thread>


#include "Scheduler.h"
#include "ThreadWrapper.h"
#include "Task.h"
#include "Future.h"

#ifndef THREADPOOL_H
#define THREADPOOL_H

class ThreadPool
{
    friend class ThreadWrapper;
    public:

        /**
         * Creates a new ThreadPool.
         * @param numWorkers <code>long int</code> the thread pools number of workers.
         * @param initialTask <code>Task *</code> the pools initial task.
         */
        ThreadPool(int numWorkers, Task * initialTask);

        /**
         * Default constructor
         * @param initialTask <code>Task *</code> the initial task.
         * Creates a thread pool with as much workers as available processors.
         */
        ThreadPool(Task * initialTask);

        /**
         * Gets the initial thread pool task future.
         * @return <code>Future *</code> the initial tasks future.
         */
        Future * getFuture(void);

        /**
         * Gets the number of workers.
         * @return <code>long int</code> the number of workers.
         */
        int getNumWorkers(void);

        /**
         * Gets the thread pool's scheduler.
         * @return <code>Scheduler *</code> the thread pool's scheduler.
         */
        Scheduler * getScheduler(void) {
            return this->sched;
        }

        /**
         * Default destructor
         */
        virtual ~ThreadPool(void);

    protected:

    private:

        /**
         * Initializes the thread pool.
         * @param numWorkers <code>long int</code> the thread pool number of workers.
         * @param initialTask <code>Task *</code> the thread pools initial task.
         */
        void init(int numWorkers, Task * initialTask);

        //The random number generator.
        RandomNumberGenerator * prng;

        //The thread pool's scheduler.
        Scheduler * sched;

        //The workers.
        ThreadWrapper ** threads;

        //The thread pools worker pthreads.
        pthread_t* pthreads;

        //The number of workers.
        int numWorkers;

        //The thread pools initial task.
        Task * initialTask;
};

#endif // THREADPOOL_H
