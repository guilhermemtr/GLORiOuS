#include "Task.h"
#include "DataTask.h"

Future * Task::submit(DataTask * t)
{
    /**
     * To solve possible future problems regarding Data task deallocation,
     * maybe creating new tasks and enqueuing them would be a solution.
     */
    if(!t->numParallelParts) t->getFuture()->end();
    else for(int i = 0; i < t->numParallelParts; i++) {
        this->worker->sched->scheduleTask(this->worker->workerId, this->worker->generator, t);
    }
    return t->getFuture();
}
